#!/bin/bash

cd /var/www
cp .env.example .env

chmod 775 .env

composer install
php artisan key:generate

/root/development-environment/payload/setup.sh
mysql -u root -p"secret" -e "DROP DATABASE IF EXISTS emeraldDragon; \
                                CREATE DATABASE emeraldDragon; \
                                CREATE USER 'emeraldDragon'@'%' IDENTIFIED BY 'Dupa123'; \
                                GRANT ALL PRIVILEGES ON * . * TO 'emeraldDragon'@'%'; \
                                FLUSH PRIVILEGES;"

# Laravel
chmod -R o+w /var/www/storage >/dev/null
php artisan migrate
php artisan db:seed

/usr/sbin/apache2ctl -D BACKGROUND
/bin/bash
